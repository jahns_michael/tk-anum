function [A1,A2,A3,A4,b1,b2,b3,b4,arcp,aarcp,arpp,aarpp,arnp,aarnp,tcp,tpp,tnp] = main()
  
  tcp = zeros(4,1);
  tpp = zeros(4,1);
  tnp = zeros(4,1);
  
  % Generate random matrices
  [A1, t1, b1] = randomnumber();
  [A2, t2, b2] = randomnumber();
  [A3, t3, b3] = randomnumber();
  [A4, t4, b4] = randomnumber();
  
  [n,n] = size(A1);
  rcp = zeros(n,4);
  rpp = zeros(n,4);
  rnp = zeros(n,4);
  
  % Generate CP solution
  [p1, q1, Lcp1, Ucp1, tcp(1,1)] = LU_CP(A1);
  [xcp1, t] = FEBS_CP(p1, q1, Lcp1, Ucp1, b1); tcp(1,1) = tcp(1,1) + t;
  [p2, q2, Lcp2, Ucp2, tcp(2,1)] = LU_CP(A2);
  [xcp2, t] = FEBS_CP(p2, q2, Lcp2, Ucp2, b2); tcp(1,1) = tcp(1,1) + t;
  [p3, q3, Lcp3, Ucp3, tcp(3,1)] = LU_CP(A3);
  [xcp3, t] = FEBS_CP(p3, q3, Lcp3, Ucp3, b3); tcp(1,1) = tcp(1,1) + t;
  [p4, q4, Lcp4, Ucp4, tcp(4,1)] = LU_CP(A4);
  [xcp4, t] = FEBS_CP(p4, q4, Lcp4, Ucp4, b4); tcp(1,1) = tcp(1,1) + t;
 
  % Generate PP solution
  [Lpp1, Upp1, xpp1, tpp(1,1)] = LSPP(A1, b1);
  [Lpp2, Upp2, xpp2, tpp(2,1)] = LSPP(A2, b2);
  [Lpp3, Upp3, xpp3, tpp(3,1)] = LSPP(A3, b3);
  [Lpp4, Upp4, xpp4, tpp(4,1)] = LSPP(A4, b4);
  
  % Generate NP solution
  [xnp1, tnp(1,1)] = LSNP(A1,b1);
  [xnp2, tnp(2,1)] = LSNP(A2,b2);
  [xnp3, tnp(3,1)] = LSNP(A3,b3);
  [xnp4, tnp(4,1)] = LSNP(A4,b4);
  
  % RE CP
  rcp(:,1) = b1 - A1*xcp1;
  rcp(:,2) = b2 - A2*xcp2;
  rcp(:,3) = b3 - A3*xcp3;
  rcp(:,4) = b4 - A4*xcp4;
  arcp = mean(rcp);
  aarcp = mean(arcp');
  
  % RE PP
  rpp(:,1) = b1 - A1*xpp1;
  rpp(:,2) = b2 - A2*xpp2;
  rpp(:,3) = b3 - A3*xpp3;
  rpp(:,4) = b4 - A4*xpp4;
  arpp = mean(rpp);
  aarpp = mean(arpp');
  
  % RE NP
  rnp(:,1) = b1 - A1*xnp1;
  rnp(:,2) = b2 - A2*xnp2;
  rnp(:,3) = b3 - A3*xnp3;
  rnp(:,4) = b4 - A4*xnp4;
  arnp = mean(rnp);
  aarnp = mean(arnp');
  
endfunction