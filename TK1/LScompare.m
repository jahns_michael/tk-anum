[A, test1, b] = randomnumber()
[xNP, tNP] = LSNP(A,b)
[xPP, tPP] = LSPP(A,b)

[errorNP] = errorCalc(A, xNP, b);
[errorPP] = errorCalc(A, xPP, b);

disp("PP Residual:");
disp(errorNP);
disp("NP Residual:");
disp(errorPP);