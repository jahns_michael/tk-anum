function [x, t] = FEBS_CP(p, q, L, U, b)
  % Gaussian Elimination for LU Factorized Complete Pivoted Matrix
  % Forward Elimination and Backward Substitution
  
  % INPUT
  % p stands for permutation vector of the rows
  % q stands for permutation vector of the columns
  % L stands for lower triangular matrix factor of A
  % U stands for upper triangular matrix factor of A
  % b stands for right-hand side vector from the linear system
  
  % OUTPUT
  % x stands for the solution of the linear system
  tic
  
  % Rearrange b
  n = length(b);
  b = b(p,:);
  
  % Forward Elimination (Ly = b)
  y = zeros(n,1);
  y(1) = b(1)/L(1,1);
  for i=2:n
    y(i) = (b(i) - L(i,1:i-1)*y(1:i-1)) / L(i,i);
  endfor
  
  % Backward Substitution (Ux = y)
  x = zeros(n,1);
  x(n) = y(n)/U(n,n);
  for i=n-1:-1:1
    x(i) = (y(i)-U(i,i+1:n)*x(i+1:n))/U(i,i);
  endfor
  
  % Rearrange x
  i = 1;
  n = length(x);
  qx = [q' x];
  while i<=n
    tmp_i = qx(i,1);
    if tmp_i == i
      i=i+1;
    else
      % swapping q and x
      tmp_qx = qx(i,:);   
      qx(i,:) = qx(tmp_i,:);   
      qx(tmp_i,:) = tmp_qx;
    endif
  endwhile
  x = qx(:,2);
  
  t = toc;
  
endfunction