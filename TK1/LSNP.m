function [x, t] = LSNP(A, b)
tic
% LU decomposition
[n,n] = size(A);
L = eye(n);
for k=1:n-1
    L(k+1:n,k) = A(k+1:n,k)/A(k,k);
    for i=k+1:n
        A(i,k:n) = A(i,k:n) - L(i,k) * A(k,k:n);
    end
end
U = A;
% Forward elimination
y = zeros(n,1);
for i=1:n
    y(i) = b(i) / L(i,i);
    for j = i+1:n
        b(j) = b(j) - L(j,i) * y(i);
    end
end
% Backward substitution
x = zeros(n,1);
for i = n:-1:1
    x(i) = y(i) / U(i,i);
    for j=1:i-1
        y(j) = y(j) - U(j,i) * x(i);
    end
end
t = toc;
endfunction