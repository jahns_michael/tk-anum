function [L, U, x, t] = LSPP(A,b)
tic
% LUP decomposition
[n,n] = size(A);
L = eye(n);
P = eye(n);
for i=1:n
    [pivot m] = max(abs(A(i:n,i)));
    m = m+i-1;
    if m ~= i
        A([m,i],:) = A([i,m],:);
        P([m,i],:) = P([i,m],:);
        if i>=2
            L([m,i],1:i-1) =  L([i,m], 1:i-1);
        end
    end
    for j = i+1:n
        L(j,i) = A(j,i) / A(i,i);
        A(j,:) = A(j,:) - L(j,i) * A(i,:);
    end
end
U = triu(A);
b = P * b;
% Forward elimination
y = zeros(n,1);
for i=1:n
    y(i) = b(i) / L(i,i);
    for j = i+1:n
        b(j) = b(j) - L(j,i) * y(i);
    end
end
% Backward substitution
x = zeros(n,1);
for i = n:-1:1
    x(i) = y(i) / U(i,i);
    for j=1:i-1
        y(j) = y(j) - U(j,i) * x(i);
    end
end
t = toc;
endfunction