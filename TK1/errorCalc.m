function residual = residualCalc(A, X, B)
  residual = B - A * X;
endfunction

function errorEstimate = errorCalc(A, X, B)
  errorEstimate = norm(inv(A)) * norm(residual(A, X, B));
endfunction

function error = errorCalc2(XControl, X)
  error =  XControl - X;
endfunction
