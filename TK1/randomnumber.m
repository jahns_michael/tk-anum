function [A, testcase, b] = randomnumber()
A = randi([1,100],8,8);
testcase = randi([1,20],8,1);
b = A*testcase;
endfunction