function [p, q, L, U, t] = LU_CP(A)
  % LU Factorization with Complete Pivoting
  % reference : (...)
  % (...) https://www.mathworks.com/matlabcentral/fileexchange/ (...)
  % (...) 25758-gaussian-elimination-using-complete-pivoting
  
  % INPUT
  % A is the n*n input matrix
  
  % OUTPUT
  % p stands for permutation vector of the rows
  % q stands for permutation vector of the columns
  % L stands for lower triangular matrix factor of A
  % U stands for upper triangular matrix factor of A
  
  % INITIAL DECLARATION
  tic
  
  [n n] = size(A);               % n is the size of A
  L = zeros(n);
  p = 1:n;                       % p = [i] for i=1 to n
  q = 1:n;                       % q = [i] for i=1 to n
  
  
  % MAIN ITERATION
  for k=1:n-1
    % choosing the pivot using complete pivoting.
    % choosing the greatest absolute value of ...
    % the k-th row and k-th columns.
    % coordinates = (ir, ic)
    [m ri] = max(abs(A(p(k:n), q(k:n))));
    [m ci] = max(m);
    ic = ci; ir = ri(ci);
    
    % transform the coordinates (ir, ic) to ...
    % the coordinates of A
    ir = ir + (k-1);
    ic = ic + (k-1);
    
    p([k ir])=p([ir k]);
    q([k ic])=q([ic k]);
    
    % GE
    for i=k+1:n 
        L(p(i),q(k))=A(p(i),q(k))/A(p(k),q(k));
        A(p(i),:)=A(p(i),:)-L(p(i),q(k))*A(p(k),:);
    end
    
  endfor
  
  % OUTPUT ASSIGNMENT
  L=L(p,q) + eye(n);
  U=A(p,q);
  t=toc;
  
  
endfunction

