function alpha = armijo(x, p)
  % ref : https://en.wikipedia.org/wiki/Backtracking_line_search
  c = 0.25;
  tau = 0.5;
  m = gradF(x)'*p;
  t = -1*c*m;
  alpha = 1;
  while (F(x)-F(x+alpha*p) <= alpha*t)
    alpha = tau*alpha;
  endwhile
endfunction
