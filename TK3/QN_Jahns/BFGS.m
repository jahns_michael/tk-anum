function B_out = BFGS(B, dx, dg)
  % ref : https://en.wikipedia.org/wiki/Broyden-Fletcher-Goldfarb-Shanno_algorithm
  B_out = B + ((dg*dg')/(dg'*dx)) - ((B*dx*dx'*B')/(dx'*B*dx));
endfunction
