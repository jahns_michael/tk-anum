function [out] = QuasiNewton(in)
  TOL = 10^-5;
  
  x = in;
  [m, n] = size(x);
  B = eye(n);
  k = 1;
  
  while norm(gradF(x)) >= TOL
    p = -1*B*gradF(x);
    p = p/norm(p);
    
    alpha = armijo(x,p);
    dg = gradF(x);
    x = x + alpha*p;
    dg = gradF(x)-dg;

    B = BFGS(B, alpha*p, dg);
    OUT(k,1) = k;
    for i=2:n+2
      OUT(k,i) = x(i-1);
    endfor
    k = k+1;
  endwhile
  
  out=x;
  OUT
endfunction