function alph = armijosym(x, p, fa)
  syms x1 x2 alf;
  f = 10 + (x1-2)^2 + (x2+5)^2;
  c = 0.25;
  tau = 0.5;
  m = p*p';
  t = -1*c*m;
  alp = 0.5;
  while (subs(f, [x1 x2], [x(1) x(2)]) - subs(fa, alf, alp) < alp*t)
    alp = tau * alp;
    display(alp);
  end
  alph = alp;
end