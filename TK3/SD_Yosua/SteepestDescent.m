function [res] = SteepestDescent(x, TOL)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    n = 1
    k = x;
    display(k);
    p = -(jacobi(k));
    fa = f_alpha(k, p);
    alp = armijosym(k, p, fa);
    x = k + alp * p;
    while n < 20
        n = n + 1
        k = x;
        display(k);
        p = -(jacobi(k));
        fa = f_alpha(k, p);
        alp = armijosym(k, p, fa);
        x = k + alp * p;
    end
    res = x;
end