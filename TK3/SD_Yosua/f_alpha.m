function [fa] = f_alpha(x, p)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    syms x1 x2 alf;
    f = 10+(x1-2)^2+(x2+5)^2;
    fa = subs(f, [x1 x2], [x(1) + alf * p(1), x(2) + alf * p(2)]);
end