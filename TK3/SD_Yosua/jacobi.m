function [p] = jacobi(x)
    %jacobi Summary of this function goes here
    %   Detailed explanation goes here
    syms x1 x2;
    f = 10+(x1-2)^2+(x2+5)^2;
    jac = jacobian(f);
    p(1) = subs(jac(1), [x1 x2], [x(1) x(2)]);
    p(2) = subs(jac(2), [x1 x2], [x(1) x(2)]);
end