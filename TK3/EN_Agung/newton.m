function out = newton(in)
  TOL = 10^-5;
  x=in;
  H=[2 0;0 2]
  while norm(gradF(x)) >= TOL
    v=H\(-1*gradF(x));
    x=x+v;
  endwhile
  out=x;
endfunction
