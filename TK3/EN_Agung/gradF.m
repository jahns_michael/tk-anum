function g = gradF(in)
  x = in(1,1);
  y = in(2,1);
  g = [2*x-4;2*y+10];
endfunction
