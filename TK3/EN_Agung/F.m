function f = F(in)
  x = in(1,1);
  y = in(2,1);
  f = 10+(x-2)^2+(y+5)^2;
endfunction
