function x = FEBS(L, U, b)
  % Forward elimination
  [n,n] = size(L);
  
  y = zeros(n,1);
  for i=1:n
    y(i) = b(i) / L(i,i);
    for j = i+1:n
      b(j) = b(j) - L(j,i) * y(i);
    endfor
  endfor
  
  % Backward substitution
  x = zeros(n,1);
  for i = n:-1:1
    x(i) = y(i) / U(i,i);
    for j=1:i-1
      y(j) = y(j) - U(j,i) * x(i);
    endfor
  endfor
  
endfunction