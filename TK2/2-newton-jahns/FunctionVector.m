function xout = FunctionVector(xin)
  x = xin(1,1);
  y = xin(2,1);
  xout(1,1) = x*x - 3*x*y*y + 8*y - 100;
  xout(2,1) = cos(pi*x) - x*y*y + 35;
endfunction
