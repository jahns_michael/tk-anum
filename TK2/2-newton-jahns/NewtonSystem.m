function [xout] = NewtonSystem(xin, max_iter)  
  % function call and contruct Jacobian matrix
  f = FunctionVector(xin);
  J = JacobianMatrix(xin);
  % [L,U] = LU(J);
  
  % main iter
  for i=1:max_iter
    % d = FEBS(L, U, (-1*f));
    d = J\(-f);
    xin = xin + d;
    
    xin % print
    
    f = FunctionVector(xin);
    J = JacobianMatrix(xin);
  endfor
  
  % output assignment
  xout = xin;
endfunction
