function J = JacobianMatrix(xin)
  n = length(xin);
  h = 10^-5;
  J = zeros(n,n);
  for i=1:n
    for j=1:n
      xh = xin;
      xh(j) = xh(j) + h;
      fh = FunctionVector(xh);
      f = FunctionVector(xin);
      deltaFi = fh(i)-f(i);
      J(i,j) = deltaFi/h;
    endfor
  endfor
endfunction
