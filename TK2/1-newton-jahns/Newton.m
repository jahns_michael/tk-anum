function [x,n] = Newton(x0,TOL,OPT)
  % init iter count
  n = 0;
  
  % format TOL
  TOL = 10^(-1*TOL);
  
  % function call
  f0 = FunctionCall(x0,OPT);
  
  % main iter
  while abs(f0)>TOL
    df0 = dFunctionCall(x0,OPT);
    x0 = x0 - f0/df0;
    f0 = FunctionCall(x0,OPT);
    n = n + 1;
  endwhile
  
  % output assignment
  x = x0;
endfunction
