function xn = TestNewton(x0,OPT)
  nvec=zeros(1,5);
  xvec=zeros(1,5);
  TOL=[4 6 8 10 12];
  for i=1:5
    [xvec(1,i),nvec(1,i)]=Newton(x0,TOL(1,i),OPT);
  endfor
  xn = [xvec;nvec;];
endfunction
