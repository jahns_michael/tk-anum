function df = dFunctionCall(x0, opt)
  
  % Equation opt 1 :           x^3 + 2*x^2 + 10*x - 10 = 0
  % Equation opt 2 :                             x^x = 2
  % Equation opt 3 :     10*exp(-x)*sin(2*phi*x) - 2 = 0
  % Equation opt 4 : x^4 - 6*x^3 + 12*x^2 - 10*x + 3 = 0
  % Equation opt 5 :                x^2 - cos(phi*x) = 0
  
  x = x0;
  
  if (opt==1)
    df = 3*x*x + 4*x + 10;
  elseif (opt==2)
    df = x^x * (log(x) + 1);
  elseif (opt==3)
    df = -exp(-x)*(10*sin(2*pi*x) - 20*pi*cos(2*pi*x));
  elseif (opt==4)
    df = 4*x^3 - 18*x^2 + 24*x - 10;
  elseif (opt==5)
    df = pi*sin(pi*x) + 2*x;
  elseif (opt==6)
    df = 6*((2*x+1)^2);
  endif
endfunction
