function [x,n] = FiniteDifference(x0,TOL,OPT)
  % init iter count
  n = 0;
  
  % choose h as a small number
  h = 10^(-5);
  
  % function call
  f0 = FunctionCall(x0,OPT);
  
  % main iter
  while abs(f0)>TOL
    fh = FunctionCall(x0+h,OPT);
    df0 = (fh-f0)/h;
    x0 = x0 - f0/df0;
    f0 = FunctionCall(x0,OPT);
    n = n + 1;
  endwhile
  
  % output assignment
  x = x0;
endfunction
