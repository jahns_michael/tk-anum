import numpy as np

def a(x):
    return x**3 + 2*(x**2) + 10*x - 20

def da(x):
    return 3*(x**2) + 4*x + 10

def b(x):
    return x**x - 2

def db(x):
    return x**x * (np.log(x) + 1)

def c(x):
    return 10 * np.exp(-1*x) * np.sin(2*np.pi*x) - 2

def dc(x):
    return 0 #belum implementasi

def d(x):
    return x**4 - 6*x**3 + 12*x**2 - 10*x + 3

def dd(x):
    return 4*x**3 - 18*x**2 + 24*x - 10

def e(x):
    return x**2 - np.cos(np.pi*x)

def de(x):
    return 2*x + np.pi*sin(np.pi*x)

def func(x, opt):
    if (opt == 1):
        f = a(x)
    elif (opt == 2):
        f = b(x)
    elif (opt == 3):
        f = c(x)
    elif (opt == 4):
        f = d(x)
    elif (opt == 5):
        f = e(x)
    return f

def dfunc(x, opt):
    if (opt == 1):
        df = da(x)
    elif (opt == 2):
        df = db(x)
    elif (opt == 3):
        df = dc(x)
    elif (opt == 4):
        df = dd(x)
    elif (opt == 5):
        df = de(x)
    return df

def newton(x, t, opt):
    epsilon = 10**t

    i = 0
    f = funct(x, opt)
    df = dfunct(x, opt)
    h = f/df

    while(h-epsilon != 0):
        i = i+1
        x = x-h
        f = funct(x, opt)
        df = dfunct(x, opt)
        h = f/df
        
    
