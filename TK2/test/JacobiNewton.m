function [w] = JacobiNewton(w0,Tol)
  x = w0(1,1);
  y = w0(2,1);
  
  F = zeros(2,1);
  F(1,1) = (x^2)*y - 3*(x^2) + 4*x - y;
  F(2,1) = x*sin(y) - 1;
  
  J = zeros(2,2);
  J(1,1) = 2*x*y - 6*x;
  J(1,2) = x^2 + 4;
  J(2,1) = sin(y);
  J(2,2) = x*cos(y);
  
  w1 = w0;
  
  for i=1:3
    d = J\F;
    w1 = w1+d;
    
    x = w1(1,1);
    y = w1(2,1);
  
    F(1,1) = (x^2)*y - 3*(x^2) + 4*x - y;
    F(2,1) = x*sin(y) - 1;
  
    J(1,1) = 2*x*y - 6*x;
    J(1,2) = x^2 + 4;
    J(2,1) = sin(y);
    J(2,2) = x*cos(y);  
  endfor
  
  w= w1;
endfunction
