function [x,n] = bisection_algo(a,b,tol,opt)
    a_init = a;
    b_init = b;
    fa = FunctionCall(a,opt);
    fb = FunctionCall(b,opt);
    if fa*fb > 0
      printf('Interval for formula no. %f is rejected at %d to %d', opt, a, b)
      x = nan;
      n = nan;
    else
      n = 1;
      while abs(a-b) > tol
          m = a + (b-a)/2;
          fm = FunctionCall(m,opt);
          if (fa * fm) > 0
              a = m;
              fa = fm;
          else
              b = m;
              fb = fm;
          end
          n += 1;
      end
      if fix(abs(FunctionCall(m,opt))) > 0
          printf('Bisection of formula no. %f could not find approx. solution at interval %d to %d with tolerance value %e\n', opt, a_init, b_init, tol);
          x = nan;
      else
          x = m;
      endif
    endif
endfunction