% tolerance
tols = [10^-4, 10^-6, 10^-8, 10^-10, 10^-12];

% initial guess interval
ab1 = [1 1 1 0 0;2 3 4 2 3];
ab2 = [0.5 0.5 0.5 1 1;2 3 4 3 4];
ab3 = [-0.7 -0.2 0.3 0.8 1.3;-0.3 0.2 0.7 1.2 1.7];
ab4 = [0 0 0.5 0.5 2.5;2 2.5 2 2.5 3.5];
ab5 = [-1 -0.5 0 0 0.25;0 0 0.5 1 0.75];

% initial root by (interval, tolerance)
xs1 = zeros(5);
xs2 = zeros(5);
xs3 = zeros(5);
xs4 = zeros(5);
xs5 = zeros(5);

% initial number of itreation by (interval, tolerance)
ns1 = zeros(5);
ns2 = zeros(5);
ns3 = zeros(5);
ns4 = zeros(5);
ns5 = zeros(5);

% iteration
for i=1:5
  for j=1:length(tols)
    [x1 n1] = bisection_algo(ab1(1,i),ab1(2,i),tols(j),1);
    xs1(j,i) = x1;
    ns1(j,i) = n1;
    [x2 n2] = bisection_algo(ab2(1,i),ab2(2,i),tols(j),2);
    xs2(j,i) = x2;
    ns2(j,i) = n2;
    [x3 n3] = bisection_algo(ab3(1,i),ab3(2,i),tols(j),3);
    xs3(j,i) = x3;
    ns3(j,i) = n3;
    [x4 n4] = bisection_algo(ab4(1,i),ab4(2,i),tols(j),4);
    xs4(j,i) = x4;
    ns4(j,i) = n4;
    [x5 n5] = bisection_algo(ab5(1,i),ab5(2,i),tols(j),5);
    xs5(j,i) = x5;
    ns5(j,i) = n5;
  endfor
endfor
xs1
ns1
xs2
ns2
xs3
ns3
xs4
ns4
xs5
ns5