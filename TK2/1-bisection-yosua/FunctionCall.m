function f = FunctionCall(x0, opt)
  
  % Equation opt 1 :           x^3 + 2*x^2 + 10*x - 10 = 0
  % Equation opt 2 :                             x^x = 2
  % Equation opt 3 :     10*exp(-x)*sin(2*phi*x) - 2 = 0
  % Equation opt 4 : x^4 - 6*x^3 + 12*x^2 - 10*x + 3 = 0
  % Equation opt 5 :                x^2 - cos(phi*x) = 0
  
  x = x0;
  
  if (opt==1)
    f = x*x*x + 2*x*x + 10*x - 20;
  elseif (opt==2)
    f = x^x -2;
  elseif (opt==3)
    f = 10*exp(-x)*sin(2*pi*x) - 2;
  elseif (opt==4)
    f = x^4 - 6*x^3 + 12*x^2 - 10*x + 3;
  elseif (opt==5)
    f = x^2 - cos(pi*x);
  endif
endfunction
