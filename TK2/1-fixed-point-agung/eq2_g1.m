function ret = eq2_g1 (x)
  ret = 2 / (x^(x-1))
endfunction
