function ret = eq5_g3(x)
  ret = acos(x^2)/pi
endfunction