%Fungsi 1
%Tebakan
m1 = 20
m2 = 40
m3 = 1
m4 = 10
m5 = sqrt(20) 

f1_g1_to6_m3 = fixed_point("eq1_g1", 6, m3)
f1_g2_to6_m3 = fixed_point("eq1_g2", 6, m3)
f1_g3_to6_m3 = fixed_point("eq1_g3", 6, m3)


f2_g1_to6_m3 = fixed_point("eq2_g1", 6, m3)
f2_g2_to6_m3 = fixed_point("eq2_g2", 6, m3)
f2_g3_to6_m3 = fixed_point("eq2_g3", 6, m3)


f3_g1_to6_m3 = fixed_point("eq3_g1", 6, m3)
f3_g2_to6_m3 = fixed_point("eq3_g2", 6, m3)


f4_g1_to6_m3 = fixed_point("eq4_g1", 6, m3)
f4_g2_to6_m3 = fixed_point("eq4_g2", 6, m3)
f4_g3_to6_m3 = fixed_point("eq4_g3", 6, m3)


f5_g1_to6_m3 = fixed_point("eq5_g1", 6, m3)
f5_g2_to6_m3 = fixed_point("eq5_g2", 6, m3)
f5_g3_to6_m3 = fixed_point("eq5_g3", 6, m3)

%G1
%g1_to4_gs1 = fixed_point("eq1_g1", 4, 20)
%g1_to6_gs1 = fixed_point("eq1_g1", 6, 20)
%g1_to8_gs1 = fixed_point("eq1_g1", 8, 20)
%g1_to10_gs1 = fixed_point("eq1_g1", 10, 20)
%g1_to12_gs1 = fixed_point("eq1_g1", 12, 20)

%g1_to4_gs2 = fixed_point("eq1_g1", 4, 40)
%g1_to6_gs2 = fixed_point("eq1_g1", 6, 40)
%g1_to8_gs2 = fixed_point("eq1_g1", 8, 40)
%g1_to10_gs2 = fixed_point("eq1_g1", 10, 40)
%g1_to12_gs2 = fixed_point("eq1_g1", 12, 40)

%g1_to4_gs3 = fixed_point("eq1_g1", 4, 0)
%g1_to6_gs3 = fixed_point("eq1_g1", 6, 0)
%g1_to8_gs3 = fixed_point("eq1_g1", 8, 0)
%g1_to10_gs3 = fixed_point("eq1_g1", 10, 0)
%g1_to12_gs3 = fixed_point("eq1_g1", 12, 0)

%g1_to4_gs4 = fixed_point("eq1_g1", 4, 10)
%g1_to6_gs4 = fixed_point("eq1_g1", 6, 10)
%g1_to8_gs4 = fixed_point("eq1_g1", 8, 10)
%g1_to10_gs4 = fixed_point("eq1_g1", 10, 10)
%g1_to12_gs4 = fixed_point("eq1_g1", 12, 10)

%g1_to4_gs5 = fixed_point("eq1_g1", 4, sqrt(20))
%g1_to6_gs5 = fixed_point("eq1_g1", 6, sqrt(20))
%g1_to8_gs5 = fixed_point("eq1_g1", 8, sqrt(20))
%g1_to10_gs5 = fixed_point("eq1_g1", 10, sqrt(20))
%g1_to12_gs5 = fixed_point("eq1_g1", 12, sqrt(20))

%G2
%g2_to4_gs1 = fixed_point("eq1_g1", 4, 20)
%g2_to6_gs1 = fixed_point("eq1_g2", 6, 20)
%g2_to8_gs1 = fixed_point("eq1_g2", 8, 20)
%g2_to10_gs1 = fixed_point("eq1_g2", 10, 20)
%g2_to12_gs1 = fixed_point("eq1_g2", 12, 20)

%g2_to4_gs2 = fixed_point("eq1_g2", 4, 40)
%g2_to6_gs2 = fixed_point("eq1_g2", 6, 40)
%g2_to8_gs2 = fixed_point("eq1_g2", 8, 40)
%g2_to10_gs2 = fixed_point("eq1_g2", 10, 40)
%g2_to12_gs2 = fixed_point("eq1_g2", 12, 40)

%g2_to4_gs3 = fixed_point("eq1_g2", 4, 0)
%g2_to6_gs3 = fixed_point("eq1_g2", 6, 0)
%g2_to8_gs3 = fixed_point("eq1_g2", 8, 0)
%g2_to10_gs3 = fixed_point("eq1_g2", 10, 0)
%g2_to12_gs3 = fixed_point("eq1_g2", 12, 0)

%g2_to4_gs4 = fixed_point("eq1_g2", 4, 10)
%g2_to6_gs4 = fixed_point("eq1_g2", 6, 10)
%g2_to8_gs4 = fixed_point("eq1_g2", 8, 10)
%g2_to10_gs4 = fixed_point("eq1_g2", 10, 10)
%g2_to12_gs4 = fixed_point("eq1_g2", 12, 10)

%g2_to4_gs5 = fixed_point("eq1_g2", 4, sqrt(20))
%g2_to6_gs5 = fixed_point("eq1_g2", 6, sqrt(20))
%g2_to8_gs5 = fixed_point("eq1_g2", 8, sqrt(20))
%g2_to10_gs5 = fixed_point("eq1_g2", 10, sqrt(20))
%g2_to12_gs5 = fixed_point("eq1_g2", 12, sqrt(20))