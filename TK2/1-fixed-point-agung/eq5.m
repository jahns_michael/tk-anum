function ret = eq5 (x)
  ret = x^2 - cos(pi * x)
endfunction
