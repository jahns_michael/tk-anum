function ret = eq4_g2(x)
  ret = nthroot(6*x^3 - 12*x^2 + 10*x - 3, 4)
endfunction