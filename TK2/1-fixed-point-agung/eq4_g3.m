function ret = eq4_g3 (x)
  ret = (6 * x^3 - 12 * x^2 + 10 * x - 3) / x^3 
endfunction