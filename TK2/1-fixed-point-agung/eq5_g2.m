function ret = eq5_g2(x)
  ret = cos(pi * x) / x
endfunction
