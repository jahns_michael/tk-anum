function ret = eq3 (x)
  ret = 10 * e^(-x) * sin(2 * pi * x) - 2
endfunction
