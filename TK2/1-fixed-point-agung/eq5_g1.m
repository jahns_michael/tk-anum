function ret = eq5_g1(x)
  ret = sqrt(cos(pi*x))
endfunction
