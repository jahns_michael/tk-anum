function ret = eq4_g1(x)
  ret = -3/(x^3 - 6*x^2 + 12*x - 10)
endfunction