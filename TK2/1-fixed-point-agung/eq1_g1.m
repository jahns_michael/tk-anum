function ret = eq1_g1 (x)
  ret = 20 / (x^2 + 2 * x + 10)
endfunction
