% params(func:string, tol:int, guess:int) 

function [outputset] = fixed_point(func, tol, guess)
  tolerance = 1 * 10^(-tol)
  limiter = 0;
  prevoutput = 0
  output = guess
  f = str2func(func)
  arr = [output]
  while(abs(abs(output) - abs(prevoutput)) > tolerance)
    if(limiter > 10)
      break
    endif
    prevoutput = output
    output = f(output)
    arr = [arr output]
  endwhile
  outputset = arr
endfunction
