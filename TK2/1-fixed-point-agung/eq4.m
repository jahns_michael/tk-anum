function ret = eq4 (x)
  ret = x^4 - 6 * x^3 + 12 * x^2 - 10 * x + 3
endfunction
