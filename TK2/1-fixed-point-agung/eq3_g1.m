function ret = eq3_g1 (x)
  ret = log(10 * sin(2*pi*x) / 2)
endfunction